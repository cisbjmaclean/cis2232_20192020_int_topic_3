package info.hccis.teetimebooker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeeTimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeeTimeApplication.class, args);
	}

}
