package info.hccis.demo.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BaseController {



    @RequestMapping("/")
    public String home2() {
        
        //Action would do something
        System.out.println("this is a very basic interaction with the model...  not really doing anything");
        
        return "index";
    }

    
}
