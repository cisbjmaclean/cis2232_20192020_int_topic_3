package info.hccis.demo.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        System.out.println("Starting to run the program.");
        SpringApplication.run(DemoApplication.class, args);
    }

}
